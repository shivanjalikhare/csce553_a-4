import java.io.BufferedReader;

import java.io.FileReader;
import java.io.IOException;

public class StrategyPatternDemo {

	public static void main(String[] args) throws IOException {
		// int count = args.length;

		try {
			String operation = args[0];

			if (args.length == 2) {
				String file = args[1];

				if (operation.equalsIgnoreCase("wc")) {
					Context context = new Context(new OperationCount());
					FileRead(file, context);
					context.output();
				} else if (operation.equalsIgnoreCase("freq")) {
					Context context = new Context(new OperationFrequency());
					FileRead(file, context);
					context.output();
				}
			} else {
				String word = args[1];
				String file = args[2];
				Context context = new Context(new OperationSearch(word));
				FileRead(file, context);
				// context.output();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String FileRead(String file, Context context) {
		String OS = System.getProperty("os.name").toLowerCase();
		String pathName = "";
		if (OS.indexOf("win") >= 0) {
			pathName = "C:\\coderepository\\A3\\src\\main\\resources\\";
		} else {
			pathName = "src/main/resources/";
		}
		// String pathName = "C:\\coderepository\\A3\\src\\main\\resources\\";
		try {
			BufferedReader reader = new BufferedReader(new FileReader(pathName + file + ".txt")); // .txt pathName+
			String line;
			while ((line = reader.readLine()) != null) {
				context.executeStrategy(line);
			}
			reader.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
