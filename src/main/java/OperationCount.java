
public class OperationCount implements Strategy{
	 private int charcount;
	 private int wordcount;
	 private int linecount;
	   
	 OperationCount(){
		   charcount = 0;
		   wordcount = 0;
		   linecount = 0;
		   
	   }
	
	 public int getCharcount() {
		return charcount;
	}

	public int getWordcount() {
		return wordcount;
	}

	public int getLinecount() {
		return linecount;
	}

	public void doOperation(String data){
		 String d = data;
		 linec(d);
		 wordc(d);
		 charc(d);
		 
		 
	 }
	 
	 public void linec(String d) {
		// try {
		 if (d != null && !d.isEmpty()) {
				
				linecount +=d.split("\n").length;
		 }
			 //  }
		// }
		// catch(Exception e) {
			  // e.printStackTrace();
		 //  }
	 }
	 public void wordc(String d) {
		 try {
			   if(d != null) {
				   String[] words = d.replaceAll("[\\s\\n\\r.?,;/0-9/]", " ").split(" ");
				   for(String w:words) {
					   if(!(w.isEmpty())) {
						   wordcount ++;
					   }
				   }
			   }
		 }
		 catch(Exception e) {
			   e.printStackTrace();
		   }
	 }
	 
	 public void charc(String d) {
		 try {
			   if(d != null) {
				   
				   
					   charcount += d.replaceAll("[\\s,?.;'/0-9/]", "").length();

				   
			   }
		 }
		 catch(Exception e) {
			   e.printStackTrace();
		   }
	 }
	public void printoutput() {
			     System.out.println("Number Of Chars : "+charcount);
			     System.out.println("Number Of Words : "+wordcount);
			    System.out.println("Number Of Lines : "+linecount);
	}
}		   
