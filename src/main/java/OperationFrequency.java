

import java.util.HashMap;
import java.util.Map;

public class OperationFrequency implements Strategy {
	Map<String, Integer> wordcount;
	String[] words = null;
	String line = null;
	Integer frequency = null;
	OperationFrequency(){
		wordcount = new HashMap<String, Integer>();
		}
	
	public void doOperation(String data) {
		String d = data;
		split(d);
		freqcalc(words);
	}
	
	public String[] split(String d) {
		 try {
			   if(d != null) {
				   line = d.toLowerCase();
				   words = line.split("\\s+");
			   }
		 }
		 catch(Exception e) {
			   e.printStackTrace();
		   }
		 return words;
	 }
	public void freqcalc(String[] words) {
		 try {
			 for(String indword :words) {
					Integer frequency = wordcount.get(indword);
					wordcount.put(indword,(frequency == null) ? 1 : frequency +1);
				}
		 }
		 catch(Exception e) {
			   e.printStackTrace();
		   }
	 }
	
	public void printoutput() {
		System.out.println(wordcount.size() + " distinct words:");     
        System.out.println(wordcount);
	}

	
}

		
	

