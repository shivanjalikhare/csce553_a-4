import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import org.junit.Test;
import org.junit.runners.Parameterized;
import org.junit.runner.RunWith;
import org.junit.After;
import org.junit.Before;

@RunWith(Parameterized.class)
public class OperationSearchTest {
	private String input;
	//private String line;
	public static OperationSearch searchword;
	public static OperationSearch get;//change made 
	public static OperationSearch find;//change made
	private String expectedwordpresentresult;//change made
	private String expectedwordabsentresult;//change made
	private String word;
	private String multiline;//change made
	private String searchthisword;//change made
	private String getword;//change made
	private ByteArrayOutputStream outContent;

	@Before
	public void initialize() {
		outContent = new ByteArrayOutputStream();
		searchword = new OperationSearch(word);
		get = new OperationSearch(getword);//change made
		find = new OperationSearch(searchthisword);//change made
		System.setOut(new PrintStream(outContent));
	}
	
	//change made : have added two more parameters for word not present situations
	public OperationSearchTest(String expectedwordpresentresult, String findword, String expectedwordabsentresult, String word, String multiline, String searchthisword) {
		// TODO Auto-generated constructor stub
		this.expectedwordpresentresult = expectedwordpresentresult;//change made
		this.word = findword;//change made
		this.expectedwordabsentresult = expectedwordabsentresult;//change made
		this.getword = word;//change made
		this.multiline = multiline;//change made
		this.searchthisword = searchthisword;//change made
	}

	@Parameterized.Parameters
	public static Collection WSearch() {
		return Arrays.asList(new Object[][] {
			{ "My greatest strength lies in my ability effectively to communicate", "communicate","my name is","isa","This is test for multiple lines 1\r\n" + "This is test for multiple lines 2\r\n"
					+ "This is test for multiple lines 3\r\n" + "\r\n" + "",
			"is"},//change made
			{"Pizza is yummy","yummy","The world has its end","devastation","This is test for multiple lines 1\r\n" + "This is test for multiple lines 2\r\n"
					+ "This is test for multiple lines 3\r\n" + "\r\n" + "",
			"is"},//change made
			{"I study at ULL","ULL","I live at USA","France","This is test for multiple lines 1\r\n" + "This is test for multiple lines 2\r\n"
					+ "This is test for multiple lines 3\r\n" + "\r\n" + "",
			"is"},//change made
		});
	}
	@Test
	public void wordpresenttest() throws Exception {
		searchword.doOperation(expectedwordpresentresult);
		assertEquals(expectedwordpresentresult, outContent.toString());
	}
	
	//change made : have defined test case for scenario when word is not found during search
	@Test
	public void wordabsenttest() throws Exception{
		get.doOperation(expectedwordabsentresult);
		assertNotEquals(expectedwordabsentresult, outContent.toString());
	}
	
	//change made : checking for multiple line input
	@Test
	public void multiline() throws Exception{
		find.doOperation(multiline);
		assertEquals(multiline, outContent.toString());
	}
	
	
	@After
	public void destroy() {
		outContent = null;
		searchword = null;
		get = null; //change made
		find = null;//change made
	}
}

