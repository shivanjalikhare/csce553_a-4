import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;
import org.junit.Test;
import org.junit.runners.Parameterized;
import org.junit.runner.RunWith;
import org.junit.Before;
import org.junit.After;

@RunWith(Parameterized.class)
public class OperationCountTest extends Object {
	private String input;
	public static OperationCount count;
	private int expectedresultl;
	private int expectedresultw;
	private int expectedresultc;

	@Before
	public void initialize() {
		count = new OperationCount();

	}

	public OperationCountTest(String text, Integer expectedresultLine, Integer expectedC, Integer expectedw) {
		// TODO Auto-generated constructor stub
		this.input = text;
		this.expectedresultl = expectedresultLine;
		this.expectedresultc = expectedC;
		this.expectedresultw = expectedw;
	}

	@Parameterized.Parameters
	public static Collection LCount() {
		return Arrays.asList(new Object[][] { { "Hello Hi", 1, 7, 2 }, { "This is software engineering", 1, 25, 4 },
				{ "Hello Hi \n" + "Hi", 2, 9, 3 }, { "1 \n" + "2 \n" + "3", 3, 0, 0 },
				{ "There are many variations of passages of Lorem Ipsum available,\n"
						+ " but the majority have suffered alteration in some form,\n" + " by injected humour, \n"
						+ " or randomised words which don't look even slightly believable. \n"
						+ " If you are going to use a passage of Lorem Ipsum,\n"
						+ "  you need to be sure there isn't anything embarrassing hidden in the middle of text.\n"
						+ "  All the Lorem Ipsum generators on the \n"
						+ " Internet tend to repeat predefined chunks as necessary, \n"
						+ " making this the first true generator on the Internet.\n"
						+ "  It uses a dictionary of over 200 Latin words,\n"
						+ " combined with a handful of model sentence structures, \n"
						+ " to generate Lorem Ipsum which looks reasonable.\n"
						+ "  The generated Lorem Ipsum is therefore always free from repetition,\n"
						+ " injected humour, or non-characteristic words etc.", 14, 611, 120 },
				{ null, 0, 0, 0 } });
	}

	@Test
	public void testwordcount() {
		// fail("Not yet implemented");
		count.wordc(input);

		assertEquals(expectedresultw, count.getWordcount());

	}

	@Test
	public void testLinecount() {
		// fail("Not yet implemented");
		count.linec(input);
		assertEquals(expectedresultl, count.getLinecount());
	}

	@Test
	public void testCharcount() {
		// fail("Not yet implemented");
		count.charc(input);

		assertEquals(expectedresultc, count.getCharcount());

	}

	@After
	public void destroy() {
		count = null;
	}

}
